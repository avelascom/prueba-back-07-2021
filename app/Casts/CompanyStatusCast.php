<?php

namespace App\Casts;

use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use Vocces\Company\Domain\ValueObject\CompanyStatus;

class CompanyStatusCast implements CastsAttributes
{
    /**
     * Cast the given value.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @param  string  $key
     * @param  mixed  $value
     * @param  array  $attributes
     * @return string
     */
    public function get($model, string $key, $value, array $attributes)
    {
        return CompanyStatus::fromName($value);
    }

    /**
     * Prepare the given value for storage.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @param  string  $key
     * @param  mixed  $value
     * @param  array  $attributes
     * @return string
     */
    public function set($model, string $key, $value, array $attributes)
    {
        if (is_string($value)) {
            $value = CompanyStatus::create($value);
        }

        if (!$value instanceof CompanyStatus) {
            throw new \InvalidArgumentException(
                "The given value is not an CompanySatus instance"
            );
        }

        return $value;
    }
}
