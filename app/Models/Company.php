<?php

namespace App\Models;

use App\Casts\CompanyStatusCast;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Vocces\Company\Domain\ValueObject\CompanyStatus;

class Company extends Model
{
    use HasFactory;

    public $incrementing = false;
    protected $keyType = 'string';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'status',
        'email',
        'address'
    ];

    protected $casts = [
        'status' => \string::class
    ];

}
