<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Http\Request;
use Vocces\Company\Domain\ValueObject\CompanyStatus;


class CompanyController extends Controller
{
    public function index()
    {
        return Company::all();
    }

    public function enableStatus(Request $request, Company $company)
    {
        $company->update([
            'status' => CompanyStatus::enabled()
        ]);

        return response($company, 201);
    }
}
