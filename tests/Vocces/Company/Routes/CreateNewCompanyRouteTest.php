<?php

namespace Tests\Vocces\Company\Routes;

use App\Models\Company;
use Tests\TestCase;
use Illuminate\Support\Str;
use Tests\Vocces\Company\Infrastructure\CompanyRepositoryFake;
use Vocces\Company\Application\CompanyCreator;
use Vocces\Company\Domain\ValueObject\CompanyStatus;

class CreateNewCompanyRouteTest extends TestCase
{

    /**
     * @group route
     * @group access-interface
     * @test
     */
    public function postCreateNewCompanyRoute()
    {
        /**
         * Preparing
         */
        $faker = \Faker\Factory::create();
        $testCompany = [
            'name'   => $faker->name,
            'status' => 'inactive',
            'email'  => $faker->email,
            'address'=> $faker->address
        ];

        /**
         * Actions
         */
        $response = $this->json('POST', '/api/company', [
            'name'      => $testCompany['name'],
            'email'     => $testCompany['email'],
            'address'   => $testCompany['address'],
        ]);

        /**
         * Asserts
         */
        $response->assertStatus(201)
            ->assertJsonFragment($testCompany);
    }

    /**
     * @group route
     * @group access-interface
     * @test
     */
    public function putUpdateCompanyStatus()
    {
        /**
         * Preparing
         */
        $faker = \Faker\Factory::create();

        $testCompany = [
            'id'     => Str::uuid(),
            'name'   => $faker->name,
            'status' => 'active'
        ];

        $company = Company::factory()->create([
            'id'     => $testCompany['id'],
            'name'   => $testCompany['name'],
            'status' => $testCompany['status'],
        ]);

        $response = $this->json('PUT', '/api/company/enable/' . $company->id);

        /**
         * Asserts
         */
        $response->assertStatus(201)
            ->assertJsonFragment($testCompany);
    }

    /**
     * @group route
     * @group access-interface
     * @test
     */
    public function getListCompanies()
    {
        /**
         * Preparing
         */

        /**
         * Actions
         */
        $response = $this->json('GET', '/api/company');

        /**
         * Asserts
         */
        $response->assertStatus(200);
    }
}
