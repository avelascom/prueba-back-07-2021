<?php

namespace Vocces\Company\Domain\ValueObject;

final class CompanyAddress
{

    private string $address;

    public function __construct(?string $address)
    {
        if($address)
            $this->address = $address;
        else
            $this->address = "";
    }

    public function get(): string
    {
        return $this->address;
    }

    public function __toString()
    {
        return $this->address;
    }
}
