<?php

namespace Vocces\Company\Domain\ValueObject;

final class CompanyEmail
{

    private string $email;

    public function __construct(?string $email)
    {
        if($email)
            $this->email = $email;
        else
            $this->email = "";
    }

    public function get(): ?string
    {
        return $this->email;
    }

    public function __toString()
    {
        return $this->email;
    }
}
